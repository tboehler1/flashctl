use anyhow::Result;
use serde::Deserialize;
use std::{
    collections::HashMap,
    io::Read,
    path::{Path, PathBuf},
};

use crate::device::Device;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub devices: HashMap<String, Device>,
}

impl Config {
    pub fn new() -> Result<Self> {
        let mut conf_root = if let Some(conf_dir) = dirs::config_dir() {
            conf_dir
        } else {
            dirs::home_dir().ok_or(anyhow::anyhow!("home directory can't be found"))?
        };

        let conf_path = PathBuf::from("flashctl/config.toml");
        conf_root.push(&conf_path);

        let mut etc_conf = PathBuf::from("/etc");
        etc_conf.push(&conf_path);

        log::trace!(
            "Trying to open {:?}, with fallback to {:?}",
            conf_root,
            etc_conf
        );

        Self::new_from_path(conf_root).or(Self::new_from_path(etc_conf))
    }

    pub fn new_from_path<P: AsRef<Path>>(config_file: P) -> Result<Self> {
        let mut config_fd = std::fs::File::open(config_file)?;
        let mut raw_config = String::new();
        config_fd.read_to_string(&mut raw_config)?;
        let devices = toml::from_str(&raw_config)?;

        Ok(Self { devices })
    }
}
