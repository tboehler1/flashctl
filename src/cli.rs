use clap::{Parser, Subcommand};

use crate::device::PowerState;

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum ImageSource {
    Local,
    #[allow(clippy::upper_case_acronyms)]
    HTTP,
    Stdin,
}

#[derive(Clone, Debug, clap::ValueEnum)]
pub enum Compression {
    Xz,
}

#[derive(Debug, Subcommand)]
pub enum CliCommand {
    Power {
        #[arg(index = 1)]
        state: PowerState,
    },
    #[allow(clippy::upper_case_acronyms)]
    USB {
        #[arg(index = 1)]
        state: PowerState,
    },
    Flash {
        /// Where the image is stored
        source: ImageSource,
        /// Path to image to flash
        image: String,
        #[arg(long)]
        /// Image is compressed
        compression: Option<Compression>,
    },
    PreFlash,
    PostFlash,
}

#[derive(Debug, Parser)]
pub struct Args {
    #[command(subcommand)]
    pub command: CliCommand,
    #[arg(short)]
    pub dut: String,
    #[arg(long)]
    pub config: Option<String>,
    #[arg(short, action = clap::ArgAction::Count)]
    pub verbose: u8,
}

impl Args {
    pub fn new() -> Self {
        Self::parse()
    }
}
