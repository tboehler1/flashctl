use anyhow::{Context, Result};
use clap::ValueEnum;
use indicatif::{ProgressBar, ProgressStyle};
use serde::Deserialize;

use std::{
    fs::OpenOptions,
    io::{BufReader, Seek, SeekFrom},
    io::{BufWriter, Read, Write},
    path::{Path, PathBuf},
    time::Duration,
};

use crate::cli_cmd::pitest_write;

#[derive(Clone, Copy, Debug, ValueEnum)]
pub enum PowerState {
    On,
    Off,
    Reset,
}

impl std::fmt::Display for PowerState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::On => "on",
            Self::Off => "off",
            Self::Reset => "reset",
        };
        write!(f, "{}", s)
    }
}

#[derive(Debug, Deserialize)]
pub struct USBDevice {
    pub usb_location: String,
    pub usb_port: String,
}

impl USBDevice {
    pub fn usb_power(&self, state: PowerState) -> Result<()> {
        log::info!(
            "usb: location {}, port {}, action {}",
            self.usb_location,
            self.usb_port,
            state
        );

        let usb_power_path = format!(
            "/sys/bus/usb/devices/{0}:1.0/{0}-port{1}/disable",
            self.usb_location, self.usb_port
        );
        let mut usb_power_file = OpenOptions::new()
            .write(true)
            .append(false)
            .open(&usb_power_path)
            .with_context(|| format!("Failure opening usb power path: {}", &usb_power_path))?;

        match state {
            PowerState::On => {
                log::trace!("Writing 0 to {}", &usb_power_path);
                usb_power_file.write_all(b"0").map_err(anyhow::Error::msg)
            }
            PowerState::Off => {
                log::trace!("Writing 1 to {}", &usb_power_path);
                usb_power_file.write_all(b"1").map_err(anyhow::Error::msg)
            }
            PowerState::Reset => self
                .usb_power(PowerState::Off)
                .and(self.usb_power(PowerState::On)),
        }
        .with_context(|| format!("Failure writing to usb device at path {}", &usb_power_path))?;

        usb_power_file
            .flush()
            .with_context(|| format!("Failed flushing USB power file {}", &usb_power_path))?;
        usb_power_file
            .sync_all()
            .with_context(|| format!("Failed syncing USB power file {}", &usb_power_path))?;

        Ok(())
    }
}

#[derive(Debug, Deserialize)]
pub struct Device {
    pub power_port: String,
    pub usb_devices: Vec<USBDevice>,
    pub blockdev: PathBuf,
}

impl Device {
    pub fn power(&self, state: PowerState) -> Result<()> {
        log::info!("power: port {}, state {}", self.power_port, state);
        match state {
            PowerState::On => {
                pitest_write(&self.power_port, 1)?;
            }
            PowerState::Off => {
                pitest_write(&self.power_port, 0)?;
            }
            PowerState::Reset => {
                self.power(PowerState::Off)?;
                self.power(PowerState::On)?;
            }
        }
        std::thread::sleep(Duration::from_millis(1500));

        Ok(())
    }

    pub fn usb_power(&self, state: PowerState) -> Result<()> {
        for dev in &self.usb_devices {
            dev.usb_power(state)?;
        }

        Ok(())
    }

    pub fn pre_flash(&self) -> Result<()> {
        self.power(PowerState::Off)?;
        self.usb_power(PowerState::On)?;
        self.power(PowerState::On)?;

        Ok(())
    }

    pub fn post_flash(&self) -> Result<()> {
        self.power(PowerState::Off)?;
        self.usb_power(PowerState::Off)?;
        self.power(PowerState::On)?;

        Ok(())
    }

    pub fn flash<R>(&self, mut image: R, length: Option<usize>) -> Result<()>
    where
        R: Read,
    {
        use sha2::{Digest, Sha256};

        const BUF_LEN: usize = 1024 * 1024;
        let mut buf: [u8; BUF_LEN] = [0; BUF_LEN];

        self.pre_flash()?;

        wait_for_blockdev(&self.blockdev)?;

        let mut blk = std::fs::OpenOptions::new()
            .read(true)
            .write(true)
            .append(false)
            .open(&self.blockdev)?;

        log::info!("Copying to block device {:?}", self.blockdev);

        let progress_bar = if let Some(length) = length {
            ProgressBar::new(length as u64).with_style(ProgressStyle::with_template(
                "[{elapsed_precise}] {bar} {pos:>7}/{len:7} {msg}",
            )?)
        } else {
            let pb = ProgressBar::new_spinner().with_style(ProgressStyle::with_template(
                "[{elapsed_precise}] {spinner} {msg}",
            )?);
            // tick slowly manually to avoid the spinner spinning too fast
            pb.enable_steady_tick(Duration::from_millis(100));
            pb
        }
        .with_message(format!("Copying to block device {:?}", self.blockdev));

        // TODO: this is taking way longer than dd
        let mut write_hasher = Sha256::new();
        {
            let mut blk_w = BufWriter::new(&blk);
            let mut read = image.read(&mut buf)?;

            while read > 0 {
                let read_buf = &buf[..read];
                if let Err(e) = blk_w.write_all(read_buf) {
                    progress_bar.abandon_with_message(format!("Writing failed: {:?}", e));
                    return Err(e.into());
                };
                write_hasher.update(read_buf);
                progress_bar.inc(read as u64);

                read = match image.read(&mut buf) {
                    Ok(r) => r,
                    Err(e) => {
                        progress_bar.abandon_with_message(format!("Reading failed: {:?}", e));
                        return Err(e.into());
                    }
                };
            }
        }
        progress_bar.finish();

        let sync_bar = ProgressBar::new_spinner()
            .with_message("Syncing write to DUT")
            .with_style(ProgressStyle::with_template(
                "[{elapsed_precise}] {spinner} {msg}",
            )?);
        sync_bar.enable_steady_tick(Duration::from_millis(100));
        log::info!("Syncing write to DUT");
        blk.flush()?;
        blk.sync_all()?;
        sync_bar.finish();
        let digest_written = write_hasher.finalize();

        let image_len: usize = blk
            .stream_position()?
            .try_into()
            .context("u64 doesn't fit into a usize")?;
        let hashing_bar = ProgressBar::new(image_len as u64)
            .with_message("Hashing written data")
            .with_style(ProgressStyle::with_template(
                "[{elapsed_precise}] {bar} {pos:>7}/{len:7} {msg}",
            )?);

        log::info!("Hashing written data");
        let mut read_hasher = Sha256::new();
        {
            blk.seek(SeekFrom::Start(0))?;
            let mut blk_r = BufReader::new(blk);
            let mut total_len = 0;
            let mut read;

            loop {
                read = blk_r.read(&mut buf)?;
                total_len += read;

                // only read as much as was written before
                if total_len > image_len {
                    read = read.saturating_sub(total_len.saturating_sub(image_len))
                }
                if read == 0 {
                    break;
                }

                read_hasher.update(&buf[..read]);
                hashing_bar.inc(read as u64);
            }
        }
        let digest_read = read_hasher.finalize();
        hashing_bar.finish();

        if digest_written != digest_read {
            log::trace!(
                "Hashes don't match (written, read): {:?}, {:?}",
                digest_written,
                digest_read
            );
            anyhow::bail!("Hashes of image and disk don't match");
        }

        log::info!("Done writing data");

        self.post_flash()?;

        Ok(())
    }
}

fn wait_for_blockdev<P: AsRef<Path>>(path: P) -> Result<()> {
    use std::time::Instant;

    const TIMEOUT: Duration = Duration::from_secs(30);

    let blockdev = path.as_ref();
    let time = Instant::now();
    let mut now = Instant::now();
    log::info!("Waiting for {:?}", blockdev);
    let progress_bar = ProgressBar::new_spinner()
        .with_message(format!("Waiting for {:?}", blockdev))
        .with_style(ProgressStyle::with_template(
            "[{elapsed_precise}] {spinner} {msg}",
        )?);
    progress_bar.enable_steady_tick(Duration::from_millis(100));

    while !blockdev.exists() && now.duration_since(time) < TIMEOUT {
        std::thread::sleep(Duration::from_secs(1));
        now = Instant::now();
    }

    if !blockdev.exists() {
        progress_bar.finish_with_message(format!("Blockdev {:?} not found", blockdev));
        anyhow::bail!("Timeout waiting for blockdev {:?}", blockdev);
    } else {
        progress_bar.finish_with_message(format!("Blockdev {:?} found", blockdev));
        Ok(())
    }
}
