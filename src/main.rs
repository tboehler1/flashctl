use anyhow::{Context, Result};
use liblzma::bufread::XzDecoder;
use std::path::Path;

mod cli;
mod cli_cmd;
mod config;
mod device;
mod image;

use crate::cli::{Args, CliCommand};
use crate::config::Config;

fn main() -> Result<()> {
    let args = Args::new();
    let log_level = match args.verbose {
        0 => log::Level::Warn,
        1 => log::Level::Info,
        2 => log::Level::Debug,
        _ => log::Level::Trace,
    };
    simple_logger::init_with_level(log_level)?;

    let dut = args
        .config
        .map(Config::new_from_path)
        .unwrap_or_else(Config::new)
        .context("Unable to open config file")?
        .devices
        .remove(&args.dut)
        .ok_or_else(|| anyhow::anyhow!("No device with name \"{}\" found", args.dut))?;

    match args.command {
        CliCommand::Power { state } => {
            dut.power(state)?;
        }
        CliCommand::USB { state } => {
            dut.usb_power(state)?;
        }
        CliCommand::Flash {
            source,
            image,
            compression,
        } => {
            // TODO: move opening the streams to just before writing the image
            let (image, length) = match source {
                cli::ImageSource::Local => image::from_file(Path::new(&image))?,
                cli::ImageSource::HTTP => image::from_url(url::Url::parse(&image)?)?,
                cli::ImageSource::Stdin => (image::from_stdin(), None),
            };

            if let Some(compression) = compression {
                match compression {
                    cli::Compression::Xz => {
                        let xz_dec = XzDecoder::new_parallel(image);
                        dut.flash(xz_dec, None)?;
                    }
                }
            } else {
                dut.flash(image, length)?;
            }
        }
        CliCommand::PreFlash => dut.pre_flash()?,
        CliCommand::PostFlash => dut.post_flash()?,
    }

    Ok(())
}
