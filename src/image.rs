use std::{
    fs::File,
    io::{stdin, BufRead, BufReader},
    path::Path,
};

pub fn from_url(url: url::Url) -> anyhow::Result<(Box<dyn BufRead>, Option<usize>)> {
    log::debug!("Will download image from {url}");
    let body = ureq::AgentBuilder::new().build().get(url.as_str()).call()?;
    let length = body
        .header("content-length")
        .and_then(|s| s.parse::<usize>().ok());

    Ok((Box::new(BufReader::new(body.into_reader())), length))
}

pub fn from_file(f: &Path) -> anyhow::Result<(Box<dyn BufRead>, Option<usize>)> {
    log::debug!("Will use image from file {f:?}");
    let image = File::open(f)?;
    let length = image.metadata().ok().map(|m| m.len() as usize);

    Ok((Box::new(BufReader::new(image)), length))
}

pub fn from_stdin() -> Box<dyn BufRead> {
    log::debug!("Will use image from stdin");
    Box::new(BufReader::new(stdin()))
}
