use anyhow::Result;
use std::process::{Command, Stdio};

const PITEST_NAME: &str = "piTest";

pub fn pitest_write(port: &str, value: u32) -> Result<()> {
    Command::new(PITEST_NAME)
        .arg("-w")
        .arg(format!("{},{}", port, value))
        .stdout(Stdio::null())
        .spawn()?
        .wait()?;

    Ok(())
}
